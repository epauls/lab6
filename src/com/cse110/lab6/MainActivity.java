package com.cse110.lab6;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends Activity implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener, LocationListener, com.google.android.gms.location.LocationListener {

	
	private final static int
		CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	// Milliseconds per second
	private static final int MILLISECONDS_PER_SECOND = 1000; 
	// Update frequency in seconds
	public static final int UPDATE_INTERVAL_IN_SECONDS = 1; 
	// Update frequency in milliseconds
	private static final long UPDATE_INTERVAL =
		MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS; 
	// The fastest update frequency, in seconds
	private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
	// A fast frequency ceiling in milliseconds
	private static final long FASTEST_INTERVAL =
		MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
	
	
	// Define an object that holds accuracy and frequency parameters
	LocationRequest mLocationRequest;
	boolean mUpdatesRequested;
	private SharedPreferences.Editor mEditor;
	private SharedPreferences mPrefs; 
	private LocationClient mLocationClient;
	private GoogleMap mMap;
	private TextView mLatLong;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setUpMapIfNeeded();
		mMap.setMyLocationEnabled(true);
		mLatLong = (TextView) findViewById(R.id.latLong);
		// Create the LocationRequest object
		mLocationRequest = LocationRequest.create();
		// Use high accuracy
		mLocationRequest.setPriority( LocationRequest.PRIORITY_HIGH_ACCURACY);
		// Set the update interval to 5 seconds
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		// Set the fastest update interval to 1 second 
		mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
		// Open the shared preferences
		mPrefs = getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE);
		// Get a SharedPreferences editor
		mEditor = mPrefs.edit();
		// Start with updates turned on
		mUpdatesRequested = true;
		/*
		* Create a new location client, using the enclosing class to * handle callbacks.
		*/
		mLocationClient = new LocationClient(this, this, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	@SuppressLint("NewApi")
	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the map. 
		if (mMap == null) {
			mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)) .getMap();
		// Check if we were successful in obtaining the map.
			if (mMap != null) {
		// The Map is verified. It is now safe to manipulate the map.
			} 
		}
	}



	// Define the callback method that receives location updates
	@Override
	public void onLocationChanged(Location location) {
		// Report to the UI that the location was updated
		mLatLong.setText("Lat: "+location.getLatitude()+" Long: "+location.getLongitude());
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new
		LatLng(location.getLatitude(),location.getLongitude()), 18));
	}
	
	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) { 
		/*
		 * Google Play services can resolve some errors it detects.
		 * If the error has a resolution, try sending an Intent to
		 * start a Google Play services activity that can resolve * error.
		 */
		if (connectionResult.hasResolution()) { 
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult( 
						this,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
				/*
				 * Thrown if Google Play services canceled the original * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) { // Log the error
				e.printStackTrace();
			}
		} else {
			/*
			 * If no resolution is available, display a dialog to the * user with the error.
			 */
			Toast.makeText(this, "FAILURE!", Toast.LENGTH_LONG).show();
		}
	}
	
	
	
	/*
	* Called by Location Services when the request to connect the * client finishes successfully. At this point, you can
	* request the current location or start periodic updates
	*/
	@Override
	public void onConnected(Bundle dataBundle) {
		// Display the connection status
		Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show(); 
		// If already requested, start periodic updates
		if (mUpdatesRequested) {
			mLocationClient.requestLocationUpdates(mLocationRequest, this);
		}
	}
	


	/*
	* Called by Location Services if the connection to the * location client drops because of an error.
	*/
	@Override
	public void onDisconnected() {
		// Display the connection status
		Toast.makeText(this, "Disconnected. Please re-connect.",
				Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onPause() { 
		super.onPause();
		// Save the current setting for updates
		mEditor.putBoolean("KEY_UPDATES_ON", mUpdatesRequested);
		mEditor.commit();
	}
	
	@Override
	protected void onStart() { 
		super.onStart();
		mLocationClient.connect();
	}
	
	@Override
	protected void onResume() { 
		super.onResume();
		/*
		 * Get any previous setting for location updates
		 * Gets "false" if an error occurs */
		if (mPrefs.contains("KEY_UPDATES_ON")) { 
			mUpdatesRequested =
				mPrefs.getBoolean("KEY_UPDATES_ON", false);
		// Otherwise, turn off location updates
		} else {
			mEditor.putBoolean("KEY_UPDATES_ON", false); mEditor.commit();
		} 
	}

}
